## 攻め合い

黒番です。お互いに囲（かこ）みあっています。内側の白３子か黒３子のどちらかが全滅（ぜんめつ）します。

Board::capture-race-1

上のように、おたがいに囲み合っていて、しかも内側の石が単独（たんどく）では２眼を持っていない場合、内側の石同士でまわりの空白地帯（くうはくちたい　- 「ダメ」といいます）を詰（つ）めあいます。このような状況（じょうきょう）を「攻（せ）め合い」と言います。

上の問題ではどちらもダメ（駄目）を3つづつ持っていますが、黒番なので黒が一歩先に白のダメを詰め終わります。もし白番だったらこの攻め合いは白の勝です。

---

黒の勝ちです

Board::capture-race-2

---

隅にある3つの黒い石を救え!

Board::capture-race-3
