Sometimes an internal space might seem like an eye, but actually is not.

Does black have two eyes?  You may play freely.

Board::free-falseeye

Note that black must either fill the eye or play away allowing white to capture
a stone.  Black does not have two eyes here - one eye was a **false eye**.

---

Prevent white from forcing black to have a false eye.

Board::stop-falseeye

---

Both black eyes are not yet real.

Board::make-two-real

---

Black is almost surrounded.  Ensure that there will be two eyes.

Board::edge-falseeye

---

It is often difficult for beginners to judge whether an eye is false or real.
With some practice, this can become second nature!
