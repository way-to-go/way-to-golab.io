This is called a two space jump, or **niken tobi**.

Board::two-point-jump

There are pros and cons to playing this shape instead of the one space jump.

This move provides more influence by covering more area, but it can be cut more
easily by white.

Three and four point jumps are sometimes played as well.  Those jumps are
typically played in the opening.

---

This jump is called the **knight's move**, or **keima**, since it looks like
the movement a knight can make in chess.

Board::keima

This move is often used to claim corner territory, as in the diagram.

---

This move is called a **diagonal move**, or **kosumi**.

Board::kosumi

This move trades speed for stability since the stones are practically
connected.
