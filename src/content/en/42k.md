It is black's turn with two groups getting surrounded.

Either 3 black stones or 3 white stones will be captured.

Board::capture-race-1

We call the situation above a **capturing race** since neither inner group has
two eyes and both sides are racing to capture the other.

Black is able to win the race because it is black's turn.  If it were white's
turn, white would win the race.

---

Black can win this race.

Board::capture-race-2

---

Save the 3 black stones in the corner!

Board::capture-race-3
