A questo punto, dovresti aver capito come catturare le pietre.
Tuttavia, una partita inizia senza nessuna pietra in gioco.

Diamo un'occhiata ad un normale goban 19x19.

Board::zones

Per aiutare a dare un'idea delle possibilità, spesso ci riferiamo a queste tre
aree del goban.

* Le 4 aree blu sono di solito chiamate **angoli**.
* Le 4 aree verdi sono di solito chiamate **lati**.
* L'area rossa è di solito chiamata **centro**.

Non ci sono degli effettivi confini qui; li chiamiamo così per identificare in maniere semplice
e veloce queste posizioni.

Dove dovremmo iniziare a giocare?

---

L'obbiettivo nel Go è di circondare quanto più *territorio* possibile. Con questo, intendiamo
che si vuole circondare quanto più spazio vuoto possibile.

Ci sono tre gruppi nella figura successiva, ognuno che racchiude 9 spazi - o 9 **punti**.

Chiamiamo gli spazi che sono completamente circondati **territorio**.

Board::efficiency

Diamo un sguardo al numero di pietre di cui ha bisogno ogni giocatore per avere quei punti.

* Angolo - 6 pietre
* Lato - 9 pietre
* Centro - 12 pietre

Come puoi vedere, è più efficiente cercare di fare territorio prima negli angoli, poi sui lati,
e successivamente al centro. Per questo motivo, di solito i giocatori tendono a cominciare giocando
negli angoli. E' raro vedere le prime mosse giocate nel centro.

---

Quello che segue è una partita giocata da due professionisti. Sono mostrate solo le prime
venti mosse, ma dovrebbe darti un'idea di come si può sviluppare la parte iniziale di una partita.

Board::progame

Puoi vedere come la partita si sviluppa senza nessun combattimento tra le pietre.

Con circa 20 mosse giocate, qual è il *bilancio territoriale*?

Board::power

Le aree blu rappresentano dove nero ha *influenza* (o potenziale), le aree verdi, invece,
rappresentano l'area dove è bianco ad avere *influenza*.

Come puoi vedere, tutti gli angoli e i lati appartengono, anche se ancora in maniera vaga, o a nero o a bianco.
Con il procedere della partia, entrambi i giocatori possono decidere di *invadere* la zona di influenza avversaria.

Oltre a questo, una volta che sia gli angoli che i lati sono occupati, il centro acquista maggiore importanza. Se un
giocatore investe troppo pesantemente sugli angoli e i lati, potrebbe perdere sul lungo periodo.

Queste strategie nella fase iniziale della partita prendono il nome di **fuseki**. 
