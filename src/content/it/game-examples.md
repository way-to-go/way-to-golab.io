### Il flusso della partita

Sul goban in basso, puoi osservare una vera partita che prosegue dalla prima
mossa fino alla fine.

Cliccando sul bottone Successivo (>), puoi avanzare di una mossa con una spiegazione.

Continua a cliccare fino alla fine dell'avanzamento.

Board::example-0

Questa è una partita di esempio su un goban 9x9.

Alla fine, nero ha guadagnato 28 punti e bianco 24.

Tuttavia, come puoi vedere, le pietre morte contano. Una pietra bianca è stata catturata,
quindi il territorio di bianco è diminuito di 1 punto. 

Perciò, nero ha vinto di 5 punti.

In questa partita, sin dall'inizio, nero controlla il lato sinistro e bianco il destro e
la partita finisce pacificamente.

Puoi considerare il Go come un gioco di suddivisione.

---

La prossima partita è più eccitante!

Board::example-1

Non preoccuparti se non capisci cosa sta succedendo.

Nella prima partita, entrambi i giocatori hanno solo provato a circondare gli spazi
liberi, spartendosi i territori fra loro.

In questa partita, partendo dalla quarta mossa di bianco - il taglio -, entrambi
hanno cercato di catturare le pietre avversarie. Alla fine, nero ha occupato
la parte in alto a destra e quella in basso a sinistra, mentre bianco ha ottenuto
la parte in basso a destra e l'angolo in alto a sinistra. 

Bianco ha 18 punti e nero 14.

Oltre a questo, nero ha catturato 2 pietre bianche mentre bianco ha catturato
7 pietre nere.

Nota che le due pietre nere in alto a sinistra sono morte cosi come sono perchè
non hanno nessun posto dove scappare e alla fine morirebbero. Bianco non ha
bisogno di preoccuparsi di catturare le due pietre giocando C9. Tuttavia,
quando la partita finisce, bianco può toglierle dal goban e aggiungerle
ai suoi prigionieri.

Il risultato è:

Nero: 14 - 7 = 7 punti
Bianco: 18 - 2 = 16 punti
Quindi bianco ha vinto di 9 punti.
