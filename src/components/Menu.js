import React, {useRef} from 'react';
import {Link} from 'react-router-dom';
import {includes} from 'lodash';
import {useAsync} from 'react-async';

import content from '../content';
import categorizedPages from '../categorizedPages';

export default function Menu({page, locale}) {
    const {data: categories} = useAsync({
        promiseFn: content,
        watch: `./${locale}/categories.yaml`,
        path: `./${locale}/categories.yaml`,
    });
    const closeButton = useRef(null);

    function closeMenu() {
        closeButton.current.click();
    }

    return <div id="menu">
        <div id="slide-ctrl">
            <label className="menu-btn" htmlFor="menu-tgl" ref={closeButton}>
                <i className="fas fa-bars"></i>
            </label>
        </div>
        <nav>
            <input id="nav-0" className="nav-tgl" type="radio" name="nav" defaultChecked />
            {categorizedPages.map((category, index) =>
                NavSection(category, index, categories, page, locale, closeMenu)
            )}
        </nav>
    </div>;
}

function NavSection(category, index, localizedCategories, currentPage, locale, navClick) {
    if (!localizedCategories) {
        return;
    }

    const oneBasedIndex = index + 1;

    const subNav = category.pages.map(
        page => {
            const current = page === currentPage ? 'current-sub' : '';
            return <Link to={`/${locale}/${page}`} className={`${current} sub-nav`} key={page} onClick={navClick}>
                <div className="sub-nav-title">{localizedCategories[page].title}</div>
                <div className="sub-nav-caption">{localizedCategories[page].caption}</div>
            </Link>;
        }
    );
    const isActive = includes(category.pages, currentPage);
    const activeNH = isActive ? 'active-nh' : '';
    const activeSub = isActive ? 'active-sub' : '';
    return [
        <label key={category.name} className={"nav-header " + activeNH} htmlFor={`nav-${oneBasedIndex}`}>
            {localizedCategories[category.name]}
        </label>,
        <input id={`nav-${oneBasedIndex}`} className="nav-tgl" type="radio" name="nav" key={`nav-${oneBasedIndex}`}/>,
        <div className={"sub-menu " + activeSub} key={`sub-menu-${oneBasedIndex}`}>
            <label htmlFor="nav-0" className="sub-nav">
                <i className="fas fa-arrow-left"></i> {localizedCategories[category.name]}
            </label>
            {subNav}
        </div>
    ];
}
